module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      options: {

      },
      target: {
        files: {
          'custom.css': ['style.css', 'reset.css']
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.registerTask('default', ['cssmin']);

  //after all this just run command "grunt cssmin" or "grunt" will 
  //generate custom.css minified file of both style.css and reset.css 
};